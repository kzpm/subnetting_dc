=== Subnetting

Subnetting wordt gebruikt om efficient om te gaan met ip adressen. Dus niet meer adressen vrijgeven dan strikt noodzakelijk. Ook zorgt subnetting voor fysiek gescheiden netwerken, waardoor broadcast domeinen beperkt worden.

==== Netwerk en hosts



Het subnetmasker wat bij een ip adres hoort wordt op twee manieren genoteerd:

1. 255.255.255.0
2. /24 (CIDR notatie)

Dit subnetmasker is erg belangrijk. Het subnetmasker zegt hoeveel netwerken er zijn en hoeveel hosts er in ieder netwerk zitten.

In het voorbeeld hierboven zie je dat er 24 netwerkbitjes zijn. Dit weet je omdat een blokje van 255 8 bits vertegenwoordigt. Er zijn 3 blokken, dus 3*8 = 24 netwerkbits. Bij 2 zie je de CIDR notatie. Die geeft direct aan hoeveel netwerkbits er zijn. Easypeasy!

.24 netwerkbits
[%header, cols= "2,2,2,2"]
|===
|Subnetmasker|||
|255|255|255|0
|11111111|11111111|11111111|00000000
|8|8|8|0
|===

===== Netwerkbits

Netwerkbits herken je dus aan de aaneengesloten enen `11111111 11111111 11111111 00000000`
Een subnetmasker bestaat altijd uit aaneengesloten enen. Dit houdt in dat alleen de getallen 0, 128, 192, 224, 240, 248, 252, 254 en 255 geldige waarden zijn in het subnetmasker. Kijk maar naar de tabel hieronder:

[%header, cols="1,2"]
|===
|Waarde | 8 bits
|0      | 0000 0000
|128	| 1000 0000
|192	| 1100 0000
|224	| 1110 0000
|240	| 1111 0000
|248	| 1111 1000
|252	| 1111 1100
|254	| 1111 1110
|255	| 1111 1111
|===


Deze waarde heeft met binair rekenen te maken. De hoogste waarde is 128, de laagste waarde 1.
Wanneer alle bits ingeschakeld zijn (1), is de gezamenlijke waarde dus 128 + 64 + 32+ 16 + 8 + 4 + 2 + 1 = 255

. Waarde van de bitjes
[%header, cols="1,1,1,1,1,1,1,1"]
|===
|1|1|1|1|1|1|1|1
|128|64|32|16|8|4|2|1
|===


===== Hostbits

De overgebleven bits in het subnetmasker zijn de aaneengesloten nullen `0000 0000`

de hostbits zijn belangrijk, omdat we aan de hand van het aantal hostbits de volgende feiten boven tafel krijgen:

1. Het aantal hosts per subnet
2. Het aantal subnets

Laten we eens kijken naar het netwerkmasker 255.255.255.0 (24 netwerkbits)
Er blijven dus 8 hostbits over.

Nu passen we de regel 2^8 toe. Dat is 256. Het hele subnet bestaat dus uit de range 0 - 255 (Dat zijn 256 hosts). We mogen het ip adres wat eindigt op 0 en het adres wat eindigt op 255 niet gebruiken. De `0` staat voor het netwerkadres. De `255` voor het broadcastadres.

Alle adressen tussen de 0 en de 255 worden de available hosts genoemd. Dat zijn dus de adressen 1 t/m 254.

Dus eigenlijk kun je zeggen dat de available hosts zo bepaald worden (2^8)-2.

We zien hier dat we dus een subnet hebben met 254 beschikbare hosts.

===== Vreemde netwerkmaskers

We zijn nu uitgegaan van een 24 bits netwerkmasker. Maar wat als ik maar 12 available hosts nodig heb?

We weten dat een 24 bits netwerkmasker 254 available hosts geeft. Wanneer ik het netwerkmasker kleiner maak, kom ik misschien goed uit voor 12 hosts.

Laten we eens kijken wat er gebeurt wanneer ik het masker op 25 netwerkbits zet.

.25 netwerkbits
[%header, cols="4"]
|===
|Subnetmasker|||
|255|255|255|128
|11111111|11111111|11111111|10000000
|8|8|8|1
|===
 
We houden nu 7 hostbits over. Dat geeft het volgende resultaat: (2^7)-2 = 126. We hebben nu 126 available hosts. Dat is veel meer dan 12. Laten we verder kijken.

.26 netwerkbits
[%header, cols="4"]
|===
|Subnetmasker|||
|255|255|255|192
|11111111|11111111|11111111|11000000
|8|8|8|2
|===
 
Aantal hostbits is nu 6. (2^6)-2= 62. Ook nog teveel. We zetten het masker nu op 27.

.27 netwerkbits
[%header, cols="4"]
|===
|Subnetmasker|||
|255|255|255|224
|11111111|11111111|11111111|11100000
|8|8|8|3
|===

(2^5)-2 = 30. Laten we kijken of 28 netwerkbits lukt.

.28 netwerkbits
[%header, cols="4"]
|===
|Subnetmasker|||
|255|255|255|240
|11111111|11111111|11111111|11110000
|8|8|8|4
|===

(2^4)-2 = 14. Dat lijkt genoeg. We 'verspillen' dan maar twee bruikbare hosts. 

===== Blocksize en hosts bepalen

Met blocksize bedoelen we: hoeveel hosts kan ik in een subnet zetten?

We hebben gekozen voor het subnetmasker 255.255.255.240. Oftewel /28.
We weten dat de blocksize 16 is. Immers (2^4)=16.

De netwerken die gemaakt kunnen worden zijn:

.Netwerkadressen
[%header, cols="5"]
|===
|Subnet|Netwerkadres|Eerst beschikbare host|Laatst beschikbare host|Broadcast
|1|x.x.x.0|x.x.x.1|x.x.x.14|x.x.x.15
|2|x.x.x.16|x.x.x.17|x.x.x.30|x.x.x.31
|3|x.x.x.32|x.x.x.33|x.x.x.46|x.x.x.47
|4|x.x.x.48|x.x.x.49|x.x.x.62|x.x.x.63
|5|x.x.x.64|x.x.x.65|x.x.x.78|x.x.x.79
|6|x.x.x.80|x.x.x.81|x.x.x.94|x.x.x.95
|7|x.x.x.96|x.x.x.97|x.x.x.110|x.x.x.111
|8|x.x.x.112|x.x.x.113|x.x.x.126|x.x.x.127
|9|x.x.x.128|x.x.x.129|x.x.x.142|x.x.x.143
|10|x.x.x.144|x.x.x.145|x.x.x.158|x.x.x.159
|11|x.x.x.160|x.x.x.161|x.x.x.174|x.x.x.175
|12|x.x.x.176|x.x.x.177|x.x.x.190|x.x.x.191
|13|x.x.x.192|x.x.x.193|x.x.x.206|x.x.x.207
|14|x.x.x.208|x.x.x.209|x.x.x.222|x.x.x.223
|15|x.x.x.224|x.x.x.225|x.x.x.238|x.x.x.239
|16|x.x.x.240|x.x.x.241|x.x.x.254|x.x.x.255
|===

We zien hier dus 16 blocks met elk 14 beschikbare hosts.

===== Ander voorbeeld

Stel dat je nu een netwerk nodig hebt met 5100 beschikbare hosts.

Dan volstaat het netwerk masker 24 niet meer. Immers alles met een hoger netwerkmasker dan 24, telt steeds minder hosts!

We gaan eerst kijken tot welke macht we 2 moeen verheffen om bij die 5100 in de buurt te komen.

We weten dat 2^8= 256
2^12 geeft 4096 hosts

2^13 resulteert in 8192 hosts. Er worden dan bijna 3000 ip adressen niet gebruikt, maar rekenkundig is dit de enige oplossing.

We weten dus dat we 13 hostbits vrij moeten maken.


.19 netwerkbits
[%header, cols="4"]
|===
|Subnetmasker|||
|255|255|224|0
|11111111|11111111|11100000|00000000
|8|8|3|0
|===

Om de blocksize te bepalen, kunnen we werken met 5 hostsbits in het derde octet. Dus 2^5 = 32. Onze blocksize wordt 32. 

We komen dan tot de volgende indeling:


.Netwerkadressen
[%header, cols="5"]
|===
|Subnet|Netwerkadres|Eerst beschikbare host|Laatst beschikbare host|Broadcast
|1|x.x.0.0|x.x.0.1|x.x.31.254|x.x.31.255
|2|x.x.32.0|x.x.32.1|x.x.63.254|x.x.63.255
|3|x.x.64.0|x.x.64.1|x.x.95.254|x.x.95.255
|4|x.x.96.0|x.x.96.1|x.x.127.254|x.x.127.255
|5|x.x.128.0|x.x.128.1|x.x.159.254|x.x.159.255
|6|x.x.160.0|x.x.160.1|x.x.191.254|x.x.191.255
|7|x.x.192.0|x.x.192.1|x.x.223.254|x.x.223.255
|8|x.x.224.0|x.x.224.1|x.x.255.254|x.x.255.255
|===

Je ziet dat er per block 8190 beschikbare hosts zijn.


Ik hoop dat het zo duidelijk is.



